FROM golang:alpine

ARG TERRAFORM_VERSION

RUN apk add --no-cache git bash openssh jq build-base unzip
ADD https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip /tmp/

RUN unzip /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin/
RUN rm /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip

ENTRYPOINT []